(function init() {
    var popupTemplate = $('<div>')
    .attr("id", "wordbook-popup")
    .css({
        position: "fixed",
        display: "none",
        border: "solid 0.1em rgba(0,0,0, 0.15)",
        padding: "0.5em",
        "border-radius": "0.25em",
        background: "white",
        "z-index": 100000
    });
    var popup = null;

    var hidePopup = function() {
        if (popup !== null) {
            popup.remove();
            popup = null;
        }
    }
    window.addEventListener('scroll', hidePopup, {once: true});

    var askDuden = function(text, coords) {
        chrome.runtime.sendMessage({
            "message": SEARCH_MESSAGE,
            "text": text
        }, function (response) {
            if (response.message == LINK_FOUND) {
                const message = `<b>${response.lemma}</b>: ${response.tuple}`;
                popup.css({
                    "display": "block",
                    "top": coords.bottom,
                    "left": coords.left
                }).html(message);
            } else {
                popup.css({
                    "display": "block",
                    "top": coords.bottom,
                    "left": coords.left
                }).html("<span style='color: red'>✖</span>");
            }
            document.addEventListener('selectionchange', hidePopup, {once: true});
        });
    };

    let selection = document.getSelection();
    let selectionText = selection.toString().trim();
    let executionFunction = null;
    if (!selectionText) {
        executionFunction = hidePopup.bind(this);
    } else {
        let selectionCoordinates = null;
        if ($(":focus") && ($(":focus").prop("tagName") === "INPUT" || $(":focus").prop("tagName") === "TEXTAREA")) {
            let input = $(":focus").get(0);
            selectionCoordinates = getTextBoundingRect(input, input.selectionStart, input.selectionEnd);
        } else {
            selectionCoordinates = selection.getRangeAt(0).getBoundingClientRect();
        }
        executionFunction = askDuden.bind(this, selectionText, selectionCoordinates);
        //Show wait
        popup = popupTemplate.clone().appendTo($("body"));
        popup.css({
            "display": "block",
            "top": selectionCoordinates.bottom,
            "left": selectionCoordinates.left
        }).html("<span style='color: red'>⌛</span>")
    }
    executionFunction.call();
})();
  