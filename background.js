chrome.runtime.onMessage.addListener(function (request, sender, sendResponse) {
    if (request.message !== SEARCH_MESSAGE) return;
    let text = request.text;
    fetch(`https://api.pons.com/v1/dictionary?l=dedx&q=${text}&language=en`, {
        headers: {
            "X-Secret": "16515e98cd4521f9c4e153202646c6e869f18f64f877408fe1b001658c659534"
        },
        mode: "cors"
    }).then(response => response.json()).then(
        data => {
            console.log(data);
            if (!data || !data[0].hits || !data[0].hits[0].roms) {
                sendResponse({
                    "message": NOT_FOUND
                });
            } else {
                let item = data[0].hits[0].roms[0];
                sendResponse({
                    "message": LINK_FOUND,
                    "lemma": item.headword,
                    "tuple": item.headword_full
                });
            }
        },
        error => {
            sendResponse({
                "message": NOT_FOUND
            });
        }
    );
    return true;
});
chrome.commands.onCommand.addListener(function(command) {
    var buildFileProps = function (name) {
        return {
            file: name,
            allFrames: true
        }
    }
    if (command === "search_phrase") {
        chrome.tabs.executeScript({
            code: "typeof DWB_COMMON_LIBS !== 'undefined'"
        }, function(result) {
            var launchContent = function() {
                chrome.tabs.executeScript(buildFileProps("content.js"));
            };
            if (!result[0]) {
                executeScripts(null, [
                    buildFileProps("selection-helper.js"),
                    buildFileProps("constants.js"),
                    buildFileProps("jquery-2.2.4.js"),
                    {code: "DWB_COMMON_LIBS = true"}
                ], launchContent); 
            } else {
                launchContent();
            }
        })
    }
})

function executeScripts(tabId, injectDetailsArray, outerCallback) {
    function createCallback(tabId, injectDetails, innerCallback) {
        return function () {
            chrome.tabs.executeScript(tabId, injectDetails, innerCallback);
        };
    }

    var callback = null;

    for (var i = injectDetailsArray.length - 1; i >= 0; --i)
        callback = createCallback(tabId, injectDetailsArray[i], callback || outerCallback);

    if (callback !== null)
        callback();   // execute outermost function
}